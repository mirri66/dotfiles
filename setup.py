sudo locale-gen UTF-8
sudo apt-get update
sudo apt-get --assume-yes install build-essential python-dev python-pip libsnappy-dev libmysqlclient-dev nginx
sudo apt-get --assume-yes install gfortran libopenblas-dev liblapack-dev
# opencv
sudo apt-get --assume-yes install libopencv-dev python-opencv
# matplotlib, ipython
sudo apt-get --assume-yes install ipython ipython-notebook
sudo apt-get --assume-yes install python-matplotlib
sudo apt-get --assume-yes install python-pandas
sudo apt-get --assume-yes install tesseract-ocr
# mongod server
sudo apt-get --assume-yes install mongodb-server
sudo apt-get --assume-yes install redis-server
#sudo pip install -r requirements.txt
sudo xargs -a requirements.txt -n 1 pip install # skips package if fail instead of error out
sudo cp nginx.conf /etc/nginx/nginx.conf
sudo service nginx restart